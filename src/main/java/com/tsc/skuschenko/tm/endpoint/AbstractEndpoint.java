package com.tsc.skuschenko.tm.endpoint;

import com.tsc.skuschenko.tm.api.service.IServiceLocator;
import lombok.Setter;

@Setter
public class AbstractEndpoint {

    protected IServiceLocator serviceLocator;

    public AbstractEndpoint(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}
