package com.tsc.skuschenko.tm.component;

import com.tsc.skuschenko.tm.api.service.IPropertyService;
import com.tsc.skuschenko.tm.bootstrap.Bootstrap;
import com.tsc.skuschenko.tm.command.data.BackupLoadCommand;
import com.tsc.skuschenko.tm.command.data.BackupSaveCommand;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Backup implements Runnable {

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final ScheduledExecutorService executorService =
            Executors.newSingleThreadScheduledExecutor();

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void init() {
        load();
        start();
    }

    @SneakyThrows
    private void load() {
        bootstrap.parseCommand(BackupLoadCommand.NAME);
    }

    @SneakyThrows
    @Override
    public void run() {
        bootstrap.parseCommand(BackupSaveCommand.NAME);
    }

    @SneakyThrows
    public void start() {
        @Nullable final IPropertyService propertyService =
                bootstrap.getPropertyService();
        final int timeout = propertyService.getBackupTime();
        executorService.scheduleWithFixedDelay(
                this, 0, timeout, TimeUnit.SECONDS
        );
    }

}
