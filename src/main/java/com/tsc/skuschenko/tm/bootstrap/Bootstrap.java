package com.tsc.skuschenko.tm.bootstrap;

import com.tsc.skuschenko.tm.api.repository.*;
import com.tsc.skuschenko.tm.api.service.*;
import com.tsc.skuschenko.tm.command.AbstractCommand;
import com.tsc.skuschenko.tm.component.Backup;
import com.tsc.skuschenko.tm.component.FileScanner;
import com.tsc.skuschenko.tm.endpoint.AbstractEndpoint;
import com.tsc.skuschenko.tm.enumerated.Role;
import com.tsc.skuschenko.tm.exception.system.UnknownCommandException;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.model.User;
import com.tsc.skuschenko.tm.repository.*;
import com.tsc.skuschenko.tm.service.*;
import com.tsc.skuschenko.tm.util.SystemUtil;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.Optional;
import java.util.Set;

@Getter
@Setter
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String ENDPOINT_PATH =
            "com.tsc.skuschenko.tm.endpoint";

    @NotNull
    private static final String OPERATION_FAIL = "fail";

    @NotNull
    private static final String OPERATION_OK = "ok";

    @NotNull
    private static final String TM_PID = "task-manager.pid";

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final ICommandRepository commandRepository =
            new CommandRepository();

    @NotNull
    private final ICommandService commandService =
            new CommandService(commandRepository);

    @NotNull
    private final IDataService dataService =
            new DataService(this);

    @NotNull
    private final FileScanner fileScanner =
            new FileScanner(this);

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    private final IProjectRepository projectRepository =
            new ProjectRepository();

    @NotNull
    private final IProjectService projectService =
            new ProjectService(projectRepository);

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ISessionRepository sessionRepository =
            new SessionRepository();

    @NotNull
    private final ISessionService sessionService =
            new SessionService(this, sessionRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectTaskService projectTaskService =
            new ProjectTaskService(taskRepository, projectRepository);

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService =
            new UserService(userRepository, propertyService);

    @NotNull
    private final IAuthService authService =
            new AuthService(userService, propertyService);

    private void createDefaultProjectAndTask(
            @NotNull final User user, @NotNull final String name,
            @NotNull String description
    ) {
        @NotNull final Project project =
                projectService.add(user.getId(), name, description);
        project.setUserId(user.getId());
        @NotNull final Task task =
                taskService.add(user.getId(), name, description);
        task.setProjectId(project.getId());
        task.setUserId(user.getId());
    }

    @Override
    public void exit() {
        System.exit(0);
    }

    public void init() {
        initEndpoints();
        initUsers();
        initPID();
    /*    backup.init();
        fileScanner.init();*/
    }

    private void initEndpoints() {
        @NotNull final Reflections reflections = new Reflections(ENDPOINT_PATH);
        @NotNull final Set<Class<? extends AbstractEndpoint>> classes =
                reflections.getSubTypesOf(AbstractEndpoint.class);
        classes.stream().filter(item ->
                !Modifier.isAbstract(item.getModifiers()))
                .sorted(Comparator.comparing(Class::getName))
                .forEach(item -> {
                    try {
                        registry(item.newInstance());
                    } catch (InstantiationException |
                            IllegalAccessException e) {
                        e.printStackTrace();
                    }
                });
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = TM_PID;
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initUsers() {
        @NotNull final User userTest =
                userService.create("test", "test", "text@test.ru");
        createDefaultProjectAndTask(userTest, "Test", "Test");
        @NotNull final User userAdmin =
                userService.create("admin", "admin", Role.ADMIN);
        createDefaultProjectAndTask(userAdmin, "Admin", "Admin");
    }

   /* public void parseArg(@Nullable final String arg) {
        if (!Optional.ofNullable(arg).isPresent()) return;
        @Nullable final AbstractCommand abstractCommand
                = commandService.getCommandByArg(arg);
        Optional.ofNullable(abstractCommand)
                .orElseThrow(() -> new UnknownArgumentException(arg));
        abstractCommand.execute();
    }*/

    /*public boolean parseArgs(@Nullable final String[] args) {
        if (!Optional.ofNullable(args).filter(item -> item.length > 1)
                .isPresent()) {
            return false;
        }
        @Nullable final String arg = args[0];
        parseArg(arg);
        return true;
    }*/

    public void parseCommand(@Nullable final String command) {
        if (!Optional.ofNullable(command).isPresent()) return;
        @Nullable final AbstractCommand abstractCommand
                = commandService.getCommandByName(command);
        Optional.ofNullable(abstractCommand)
                .orElseThrow(() -> new UnknownCommandException(command));
        @Nullable final Role[] roles = abstractCommand.roles();
        authService.checkRoles(roles);
        abstractCommand.execute();
    }

   /* private void registry(@Nullable final AbstractCommand command) {
        if (!Optional.ofNullable(command).isPresent()) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }*/

    private void registry(@Nullable final AbstractEndpoint endpoint) {
        if (!Optional.ofNullable(endpoint).isPresent()) return;
        @Nullable String host = propertyService.getServerHost();
        if (!Optional.ofNullable(host).isPresent()) return;
        @Nullable String port = propertyService.getServerPort();
        if (!Optional.ofNullable(port).isPresent()) return;
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    /*public void run(@Nullable final String... args) {
        logService.debug("TEST");
        logService.info("***Welcome to task manager***");
        init();
        if (parseArgs(args)) exit();
        while (true) {
            try {
                System.out.print("Enter command:");
                @NotNull final String command = TerminalUtil.nextLine();
                logService.command(command);
                parseCommand(command);
                System.out.println("[" + OPERATION_OK.toUpperCase() + "]");
            } catch (@NotNull final Exception e) {
                logService.error(e);
                System.out.println("[" + OPERATION_FAIL.toUpperCase() + "]");
            }
        }
    }*/

}
