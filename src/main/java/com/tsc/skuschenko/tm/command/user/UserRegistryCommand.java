package com.tsc.skuschenko.tm.command.user;

import com.tsc.skuschenko.tm.api.service.IAuthService;
import com.tsc.skuschenko.tm.model.User;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserRegistryCommand extends AbstractUserCommand {

    @NotNull
    private static final String DESCRIPTION = "registry user";

    @NotNull
    private static final String NAME = "registry-user";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        showParameterInfo("login");
        @NotNull final String login = TerminalUtil.nextLine();
        showParameterInfo("password");
        @NotNull final String password = TerminalUtil.nextLine();
        showParameterInfo("email");
        @NotNull final String email = TerminalUtil.nextLine();
        @NotNull final IAuthService authService =
                serviceLocator.getAuthService();
        @NotNull final User user = authService.registry(login, password, email);
        showUser(user);
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
