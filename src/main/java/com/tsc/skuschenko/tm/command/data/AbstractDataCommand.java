package com.tsc.skuschenko.tm.command.data;

import com.tsc.skuschenko.tm.api.service.IProjectService;
import com.tsc.skuschenko.tm.api.service.ITaskService;
import com.tsc.skuschenko.tm.api.service.IUserService;
import com.tsc.skuschenko.tm.command.AbstractCommand;
import com.tsc.skuschenko.tm.dto.Domain;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public abstract class AbstractDataCommand extends AbstractCommand {

    public AbstractDataCommand() {

    }

    @NotNull
    public Domain getDomain() {
        @NotNull Domain domain = new Domain();
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        domain.setUsers(serviceLocator.getUserService().findAll());
        return domain;
    }

    public void setDomain(@Nullable final Domain domain) {
        if (domain == null) return;
        @NotNull final IProjectService projectService =
                serviceLocator.getProjectService();
        projectService.clear();
        projectService.addAll(domain.getProjects());
        @NotNull final ITaskService taskService =
                serviceLocator.getTaskService();
        taskService.clear();
        taskService.addAll(domain.getTasks());
        @NotNull final IUserService userService =
                serviceLocator.getUserService();
        userService.clear();
        userService.addAll(domain.getUsers());
        serviceLocator.getAuthService().logout();
    }

}
