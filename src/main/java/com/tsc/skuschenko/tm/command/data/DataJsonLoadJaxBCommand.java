package com.tsc.skuschenko.tm.command.data;

import com.tsc.skuschenko.tm.dto.Domain;
import com.tsc.skuschenko.tm.enumerated.Role;
import com.tsc.skuschenko.tm.exception.empty.EmptyBinaryPathException;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.Optional;

public class DataJsonLoadJaxBCommand extends AbstractDataCommand {

    @NotNull
    private static final String DESCRIPTION = "load data from json file";
    @NotNull
    private static final String NAME = "data-load-json-jaxb";
    @NotNull
    private final String APPLICATION_TYPE = "application/json";
    @NotNull
    private final String CONTEXT_FACTORY = "javax.xml.bind.context.factory";
    @NotNull
    private final String CONTEXT_FACTORY_JAXB =
            "org.eclipse.persistence.jaxb.JAXBContextFactory";
    @NotNull
    private final String MEDIA_TYPE = "eclipselink.media-type";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.setProperty(CONTEXT_FACTORY, CONTEXT_FACTORY_JAXB);
        showOperationInfo(NAME);
        @Nullable final String filePath =
                serviceLocator.getPropertyService().getFileJsonPath("jaxb");
        Optional.ofNullable(filePath)
                .orElseThrow(EmptyBinaryPathException::new);
        @NotNull JAXBContext jaxbContext =
                JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller =
                jaxbContext.createUnmarshaller();
        unmarshaller.setProperty(MEDIA_TYPE, APPLICATION_TYPE);
        @NotNull final File file = new File(filePath);
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
