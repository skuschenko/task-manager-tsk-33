package com.tsc.skuschenko.tm.api.repository;

import com.tsc.skuschenko.tm.api.IRepository;
import com.tsc.skuschenko.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findByEmail(@NotNull String email);

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    User removeByLogin(@NotNull String login);

}
